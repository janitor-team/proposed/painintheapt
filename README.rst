===============
Pain in the APT
===============
-----------------------------------------------------------------
Pester people about available package updates by email or jabber.
-----------------------------------------------------------------

*Pain in the APT* pesters people about available package upgrades,
just like apticron or cron-apt. However, it does so by SMTP and XMPP
(direct or MUC/conference room or pubsub node), but also by calling
``mailx``.

What does it do?
----------------
1. updates the APT cache and checks for updates
2. sends list of available updates to Jabber contacts or a conference
   room or pubsub node immediately
3. sends list of available updates and relevant changelogs (slow) to
   email recipients
4. downloads packages, but does not install them

Messages are only sent when there is any change in either the list of
updates or in the configuration of ``painintheapt``.

Options
-------
``-c``, ``--configfile`` *configuration file*
  configuration file, (defaults: /etc/painintheapt.conf)

``-d``, ``--debug``
  print debug output to stderr

``-f``, ``--force``
  send message, even if updates did not change

``-h``, ``--help``
  print help

``-s``, ``--stampfile`` *stamp file*
  stamp file (default: /var/lib/painintheapt/stamp)

``-t``, ``--testmessage``
  send a test message only

``-v``, ``--version``
  print version

Configuration
-------------

The default configuration file is ``/etc/painintheapt.conf`` in
inifile format.

There are up to three sections, ``SMTP``, ``XMPP``, and ``MAILX``.
The keys for ``SMTP`` are ``server``, ``port``, ``username``,
``password_file``, ``from``, ``to``, ``cc``, and ``send_changes``.
The keys for ``XMPP`` are ``jid``, ``password_file``, ``to``, ``room``,
``pubsub_service``, ``pubsub_node``, and ``send_changes``.
The keys for ``MAILX`` are ``from``, ``to``, ``cc``, and
``send_changes``.
See the sample configuration for their usage.

The typical cron file is ``/etc/cron.d/painintheapt``. One may call
``painintheapt`` without arguments daily, and with the ``--force``
option weekly, to make sure the transport does work.

It is highly recommended to set the accompanying logo as Jabber avatar
for the respective user.

Resources
---------
- Homepage: https://salsa.debian.org/debacle/painintheapt

License
-------
Affero General Public License 3 or higher

Logo
----

::

     (___)
    ○(@ @)○
     -\∞/-
    /__‾__\

Maybe a cow, a gnu, or a dragon?

Author
------
Martin <debacle@debian.org>
