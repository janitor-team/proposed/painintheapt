NAME=painintheapt

all: $(NAME).8 index.html

$(NAME).8: README.rst
	cat $< \
	    | sed -e '0,/^$$/s//:Manual section: 8\n/' \
	          -e '0,/Pain.*/s//$(NAME)/' \
	    | rst2man > $@

index.html: README.rst
	rst2html $< > $@

format:
	black --line-length 96 $(NAME)

pylint:
	pylint --errors-only $(NAME)

clean:
	rm -f $(NAME).8 index.html
